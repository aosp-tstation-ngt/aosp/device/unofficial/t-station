#!/bin/bash

# $1 == init.rc
#
# This scripts modifies init.rc in recovery ramdisk as below:
#   'start ueventd' line is moved
#     from 'on early-init' section
#     to   'on boot' section ;
#
# Detail of the step
#   input line    : action
#   ----------------------------
#   on early-init : hold_space <- "comment"
#   on <other>    : hold_space <- "delete"
#   on boot       : insert COMMENT1, "start healthd" and COMMENT2
#   start healthd : if hold_space == "comment" then comment out the line.
#                   if hold_space == "delete"  then delete the line. (note1)
#   COMMENT1,2    : delete the line. (note2)
#   
#   (note1,2)
#     These step is to remove the lines(COMMENT1, "start healthd" and COMMENT2)
#     which would have been added by the previous invocation of this script.
#     ( This script might be invoked multiple times on the same init.rc. )

S="[[:space:]]"
ON="^$S*on$S+(.*)$S*$"
ON_ERLY_INIT="^$S*on$S+early-init$S*$"
ON_BOOT="^$S*on$S+boot$S*$"
HEALTHD="^$S*start$S+healthd$S*$"
COMMENT1="#@@@@ moved from late-init\(begin\)"
COMMENT2="#@@@@ moved from late-init\(end\)"
START="    start healthd"

sed -E -i \
    -e "/$COMMENT1$/d" \
    -e "/$COMMENT2$/d" \
    -e "/$ON_ERLY_INIT/{p ; s/.*/comment/ ; h ; d}" \
    -e "/$ON_BOOT/a $COMMENT1\n$START\n$COMMENT2" \
    -e "/$ON/{p ; s/.*/delete/ ; h ; d}" \
    -e "/$HEALTHD/{ x ; H ; x ; s/($HEALTHD)\ncomment/#@@@@\\1 # moved to boot/M ; s/($HEALTHD)\ndelete/delete this line/M ; }" \
    -e "/delete this line/d" \
    $1
