/*
 * Copyright (C) 2016 Fuzhou Rockchip Electronics Co. Ltd. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
note1:
  The entries below:
    /sys/devices/10091000.gpu/clock
    /sys/devices/10091000.gpu/dvfs_enable
  are dynamically changed by the kernel
  when
    /sys/devices/10091000.gpu/power/control == auto .
  So the entries must be re-written each time when performance boost is needed.

note2:
  When GPU_GOV_ENABLE is set to 0, GPU_FREQ_CLK is also set to lowest frequency by the kernel.

*/


#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define LOG_TAG "RKPowerHAL"
#include <utils/Log.h>
#include <cutils/properties.h>

#include <hardware/hardware.h>
#include <hardware/power.h>

//======================================
// define constant
//======================================
#define BUFFER_LENGTH   128
#define FREQ_LENGTH     10
#define FREQ_MAX        16

#define CPU_CLUST0_GOV_PATH "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
#define CPU_CLUST0_AVAIL_GOV "/sys/devices/system/cpu/cpu0/cpufreq//scaling_available_governors"
#define CPU_CLUST0_AVAIL_FREQ "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"
#define CPU_CLUST0_SCAL_MAX_FREQ "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"
#define CPU_CLUST0_SCAL_MIN_FREQ "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"
#define BOOSTPULSE_PATH "/sys/devices/system/cpu/cpufreq/interactive/boostpulse"

#define GPU_AVAIL_FREQ "/sys/devices/10091000.gpu/available_frequencies"
#define GPU_FREQ_CLK   "/sys/devices/10091000.gpu/clock"
#define GPU_GOV_ENABLE "/sys/devices/10091000.gpu/dvfs_enable"

#define DDR_GOV_ENABLE "/dev/video_state"

enum power_mode {
  POWER_MODE_NEUTRAL  = -1,
  POWER_MODE_LOW      = 0,
  POWER_MODE_HIGH     = 1,
};
//======================================
// struct
//======================================

struct rk_power_module {
    struct power_module base;
    pthread_mutex_t lock;
    bool            isOK;
    int             fd_boost; // for BOOSTPULSE_PATH
    int             fd_gpu_clk; // for GPU_FREQ_CLK
    int             fd_gpu_dvfs;// for GPU_GOV_ENABLE
    enum power_mode mode;
    char            cpu_freq[3][FREQ_LENGTH]; // 3==[min,mid,max]
    char            gpu_freq[3][FREQ_LENGTH]; // 3==[min,mid,max]
    int             gpu_freq_len2;          // length of gpu_freq[2]

};


//======================================
// log output
//======================================
#define RK_POWER_HW_LOG_ENABLE 0

#if RK_POWER_HW_LOG_ENABLE

#define RK_POWER_HW_LOG "sys.power.log"
static int  rk_power_log_level()
{
    char value[PROPERTY_VALUE_MAX];
    property_get(RK_POWER_HW_LOG, value, "0");
    return (int)( value[0] - '0' );
}
#define RK_LOG(msg)  if(rk_power_log_level()>0) {msg;}

#else

#define RK_LOG(msg)

#endif


//======================================
//  sysfs access
//======================================
static void sysfs_write(char *path, char *s)
{
    char buf[80];
    int len;
    int fd = open(path, O_WRONLY);

    if (fd < 0) {
        strerror_r(errno, buf, sizeof(buf));
        ALOGE("Error opening %s: %s\n", path, buf);
        return;
    }

    len = write(fd, s, strlen(s));
    if (len < 0) {
        strerror_r(errno, buf, sizeof(buf));
        ALOGE("Error writing to %s: %s %s\n", path, buf, s);
    }

    close(fd);
}

//======================================
// module initialize
//======================================
/*
  read from 'fname' into 'buff', and zero-terminate
  return
    0 : OK
    < 0 : error
 */
static int  read_buff( char const*fname, char*buff, int buff_size)
{
    int   rc = 0;
    int   fd = -1;
    int   count = 0;

    if ( ( fd = open(fname, O_RDONLY) ) < 0 ||
         ( count = read( fd, buff, buff_size-1 ) ) <= 0  ||
         count == buff_size-1 ) {  // buffer is too small.
        char err_buf[80];
        strerror_r(errno, err_buf, sizeof(err_buf));
        ALOGE("Error reading from %s fd:%d count:%d errno:%d(%s)",
              fname, fd, count, errno, err_buf);
        rc = errno ? -errno : -EFBIG;
        goto ret;
    }
    buff[count] = 0;

  ret:
    if ( fd >= 0 )
        close(fd);
    return rc;
}

/*
  return
    1 : if the governor is available
    0 : not available
    < 0 : error
 */
static int is_governor_available( char const* governer )
{
    int   rc;
    char  buff[BUFFER_LENGTH];

    // buff[] <-read from fname, and zero-terminate
    if ( ( rc = read_buff(CPU_CLUST0_AVAIL_GOV, buff, sizeof(buff)) ) < 0 )
        goto ret;

    rc = strstr(buff, governer) != NULL;
  ret:
    ALOGD("is_governor_available %s rc:%d", governer, rc);
    return rc;
}

/*
  open rkmod->[fd_boost, fd_gpu_clk, fd_gpu_dvfs]
  return
    0 : if success
    <0: if error
 */
static int open_fds( struct rk_power_module* rkmod )
{
    static bool   is_warned[3] = { 0, 0, 0 };
    static const char*const names[] = { BOOSTPULSE_PATH, GPU_FREQ_CLK, GPU_GOV_ENABLE };
    int*          fds[] = { &rkmod->fd_boost, &rkmod->fd_gpu_clk, &rkmod->fd_gpu_dvfs };
    int           rc = 0;

    for ( int i = 0 ; i < 3 ; i ++ ) {
        if ( *fds[i] < 0 ) {
            if ( ( *fds[i] = open( names[i], O_WRONLY) ) < 0 ) {
                rc = -errno;
                if ( ! is_warned[i] ) {
                    is_warned[i] = true;
                    ALOGE("Error opening %s: %d(%s)", names[i], errno, strerror(errno));
                }
            } else {
                ALOGD("%s opened", names[i]);
            }
        }
    }

    return rc;
}

/*
  str_freqs[min,mid,max][] <-- get frequencies from fname
  return
    0 : if success
    <0: if error
 */
static int  get_freqs( char const*fname, char str_freqs[3][FREQ_LENGTH] )
{
    int   rc = -1;
    char  buff[BUFFER_LENGTH];
    char* buffTmp;
    char* available_freqs[FREQ_MAX];
    int   num_freq;

    ALOGD("get_freqs %s", fname );

    // buff[] <-read from fname, and zero-terminate
    if ( ( rc = read_buff(fname, buff, sizeof(buff)) ) < 0 )
        goto ret;

    // available_freqs[] <-- split buff[]
    for ( buffTmp = buff, num_freq = 0 ;
          ( available_freqs[num_freq] = strtok( buffTmp, " \n\t" ) ) != 0 ;
          buffTmp = 0, num_freq ++ ) {
        if ( FREQ_MAX <= num_freq ) {
            ALOGE("too many frequencies");
            goto ret;
        }
        ALOGD("available freq=%s", available_freqs[num_freq] );
    }

    // str_freqs[min, mid, max] <--
    //...assume the frequencies are in ascending-order.
    strcpy( str_freqs[0], available_freqs[0] );
    strcpy( str_freqs[1], available_freqs[num_freq/2] );
    strcpy( str_freqs[2], available_freqs[num_freq - 1] );

    rc = 0;
  ret:
    return rc;
}

static void rk_power_init(struct power_module *module)
{
    struct rk_power_module*   rkmod = (struct rk_power_module*)module;
    int                       rc;

    // rkmod->cpu_freq <--
    if ( ( rc = get_freqs( CPU_CLUST0_AVAIL_FREQ, rkmod->cpu_freq ) ) < 0 ) {
        ALOGE("Error reading from %s\n", CPU_CLUST0_AVAIL_FREQ);
        goto ret;
    }

    // rkmod->gpu_freq[],gpu_freq_len2 <--
    if ( ( rc = get_freqs( GPU_AVAIL_FREQ, rkmod->gpu_freq ) ) < 0 ) {
        ALOGE("Error reading from %s\n", GPU_AVAIL_FREQ);
        goto ret;
    }
    rkmod->gpu_freq_len2 = strlen( rkmod->gpu_freq[2] );

    // rkmod->mode <--
    rkmod->mode = POWER_MODE_NEUTRAL;

    // set interactive governer
    //...check if interactive governer is available
    if ( is_governor_available("interactive") <= 0 ) {
        ALOGE("Error no interactive governer\n");
        goto ret;
    }
    //...set
    sysfs_write(CPU_CLUST0_GOV_PATH, "interactive");

    // rkmod->[fd_boost,fd_gpu_xx] <-- open
    rc = open_fds(rkmod);

    rkmod->isOK = 1;
  ret:
    ALOGD("rk_power_init rc:%d", rc);
}
//======================================
// helper functions for module interface
//======================================
// trigger boostpulse
static void trigger_boostpulse(struct rk_power_module* rkmod)
{
    int   len;
    open_fds(rkmod);

    // boostpulse
    len = write(rkmod->fd_boost, "1", 1);
    if (len <= 0) {
        // prepare for retrying next time
        close(rkmod->fd_boost);
        rkmod->fd_boost = -1;
    }

    // disable gpu dvfs
    len = write(rkmod->fd_gpu_dvfs, "0", 1);
    if (len <= 0) {
        // prepare for retrying next time
        close(rkmod->fd_gpu_dvfs);
        rkmod->fd_gpu_dvfs = -1;
    }

    // set gpu clk highest
    len = write(rkmod->fd_gpu_clk, rkmod->gpu_freq[2], rkmod->gpu_freq_len2);
    if (len < rkmod->gpu_freq_len2) {
        // prepare for retrying next time
        close(rkmod->fd_gpu_clk);
        rkmod->fd_gpu_clk = -1;
    }
}
// Modify cpu gpu to performance mode
static void performance_boost(struct rk_power_module*rkmod, enum power_mode mode)
{
    RK_LOG( ALOGI("RK performance_boost Entered!") );

    if ( rkmod->mode == mode )
        return;
    rkmod->mode = mode;

    pthread_mutex_lock(&rkmod->lock);

    //'p':performance mode(set   SYS_STATUS_PERFORMANCE flag)
    //'n':normal      mode(clear SYS_STATUS_PERFORMANCE flag)
    //... ( ref arch/arm/mach-rockchip/ddr_freq.c )
    sysfs_write(DDR_GOV_ENABLE, mode==POWER_MODE_HIGH ? "p" : "n");


    //sysfs_write(CPU_CLUST0_SCAL_MAX_FREQ, mode==POWER_MODE_HIGH ? rkmod->cpu_freq[2] : rkmod->cpu_freq[1] );
    sysfs_write(CPU_CLUST0_SCAL_MIN_FREQ, mode==POWER_MODE_HIGH ? rkmod->cpu_freq[1] : rkmod->cpu_freq[0] );
    //sysfs_write(GPU_FREQ_CLK, mode==POWER_MODE_HIGH ? rkmod->gpu_freq[2] : rkmod->gpu_freq[0] );

    pthread_mutex_unlock(&rkmod->lock);
}

// Modify cpu gpu to powersave mode
static void low_power_boost(struct rk_power_module*rkmod, enum power_mode mode)
{
    RK_LOG( ALOGI("RK low_power_boost Entered!") );

    if ( rkmod->mode == mode )
        return;
    rkmod->mode = mode;

    pthread_mutex_lock(&rkmod->lock);

    //sysfs_write(CPU_CLUST0_SCAL_MAX_FREQ, mode==POWER_MODE_LOW ? rkmod->cpu_freq[1] : rkmod->cpu_freq[2] );
    sysfs_write(CPU_CLUST0_SCAL_MIN_FREQ, mode==POWER_MODE_LOW ? rkmod->cpu_freq[0] : rkmod->cpu_freq[1] );
    //sysfs_write(GPU_FREQ_CLK, mode==POWER_MODE_LOW ? rkmod->gpu_freq[0] : rkmod->gpu_freq[1] );

    pthread_mutex_unlock(&rkmod->lock);
}


//======================================
// module interface functions
//======================================
/*performs power management actions upon the
 * system entering interactive state (that is, the system is awake
 * and ready for interaction, often with UI devices such as
 * display and touchscreen enabled) or non-interactive state (the
 * system appears asleep, display usually turned off).
 */
static void rk_power_set_interactive(struct power_module *module, int on)
{
    RK_LOG( ALOGD("%s: %d", __FUNCTION__ ,on ) );

    struct rk_power_module*   rkmod = (struct rk_power_module*)module;
    if ( ! rkmod->isOK )
        return;

    performance_boost(rkmod, on ? POWER_MODE_HIGH : POWER_MODE_LOW);
}

/*
 * (*powerHint) is called to pass hints on power requirements, which
 * may result in adjustment of power/performance parameters of the
 * cpufreq governor and other controls.
 */
static void rk_power_hint(struct power_module *module, power_hint_t hint, void *data)
{
    RK_LOG( ALOGD("%s: %d %p", __FUNCTION__, hint, data ) );

    struct rk_power_module*   rkmod = (struct rk_power_module*)module;
    if ( ! rkmod->isOK )
        return;

    switch (hint) {
    case POWER_HINT_LAUNCH:
    case POWER_HINT_INTERACTION:
        trigger_boostpulse(rkmod);
        break;

    case POWER_HINT_LOW_POWER:
        low_power_boost(rkmod, data != 0 ? POWER_MODE_LOW : POWER_MODE_HIGH );
        break;

    case POWER_HINT_SUSTAINED_PERFORMANCE:
        performance_boost(rkmod, data != 0 ? POWER_MODE_HIGH : POWER_MODE_LOW );
        break;

    case POWER_HINT_VIDEO_DECODE:
    case POWER_HINT_VSYNC:
    case POWER_HINT_VR_MODE:
        break;
    default:
        break;
    }
}

static void set_feature(struct power_module *module, feature_t feature, int __unused state)
{
    struct rk_power_module*   rkmod = (struct rk_power_module*)module;
    if ( ! rkmod->isOK )
        return;

    switch (feature) {
    default:
        ALOGW("Error setting the feature, it doesn't exist %d\n", feature);
        break;
    }
}


//======================================
// module definition
//======================================
static struct hw_module_methods_t power_module_methods = {
    .open = NULL,
};

struct rk_power_module HAL_MODULE_INFO_SYM = {
  .base = {
      .common = {
          .tag = HARDWARE_MODULE_TAG,
          //@@@@module_api_version: POWER_MODULE_API_VERSION_0_5,
          .module_api_version = POWER_MODULE_API_VERSION_0_2,
          .hal_api_version = HARDWARE_HAL_API_VERSION,
          .id = POWER_HARDWARE_MODULE_ID,
          .name = TARGET_BOARD_PLATFORM " Power HAL",
          .author = "Rockchip",
          .methods = &power_module_methods,
      },

      .init = rk_power_init,
      .setInteractive = rk_power_set_interactive,
      .powerHint = rk_power_hint,
      .setFeature = set_feature,
  },
  .lock = PTHREAD_MUTEX_INITIALIZER,
  .isOK = 0,
};
