TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := cortex-a7
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_SMP := true


# packaging ( build cache/boot/recovery/system image )
TARGET_NO_BOOTLOADER := true
#TARGET_NO_KERNEL := true
#TARGET_NO_RECOVERY := true
TARGET_NO_RADIOIMAGE := true
TARGET_USERIMAGES_USE_EXT4 := true
#BOARD_USERDATAIMAGE_PARTITION_SIZE := 10485760 # not to generate data partition image.
BOARD_BOOTIMAGE_PARTITION_SIZE := 12582912
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 16793600
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 576716800
#... This generates cache.img, which is not used.
#... But 'recovery' require that cache partition is mounted on /cache.
BOARD_CACHEIMAGE_PARTITION_SIZE := 5120000
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
#... These values are used to calculate exact image size.
#... TODO: check if the value is correct.
BOARD_FLASH_BLOCK_SIZE := 4096
#(default)BOARD_NAND_PAGE_SIZE := 2048
#(default)BOARD_NAND_SPARE_SIZE := 64


# font
SMALLER_FONT_FOOTPRINT := true
MINIMAL_FONT_FOOTPRINT := true


# blue tooth
# Some framework code requires this to enable BT
BOARD_HAVE_BLUETOOTH := false


# recovery
TARGET_RECOVERY_PIXEL_FORMAT := RGBX_8888
#TARGET_RECOVERY_UPDATER_LIBS += 
#TARGET_RECOVERY_UPDATER_EXTRA_LIBS +=
TARGET_RECOVERY_UI_LIB := librecovery_ui_tstation
TARGET_RECOVERY_FSTAB = device/unofficial/t-station/copy-files/root/fstab.rk30board.bootmode.unknown
BOARD_USES_FULL_RECOVERY_IMAGE := true


# rockchip
#... ro.board.platform
TARGET_BOARD_PLATFORM := rk312x
#... ro.product.device, ro.build.product ( TARGET_DEVICE is set to $PRODUCT_DEVICE(==t-station) )
#TARGET_DEVICE := rk312x #--> t-station
#... ro.product.board
TARGET_BOOTLOADER_BOARD_NAME := rk30board
#... used in ./hardware/rockchip/../Android.mk
TARGET_BOARD_HARDWARE := rk30board
TARGET_BOARD_PLATFORM_PRODUCT := tablet
#... maybe not used
TARGET_BOARD_PLATFORM_GPU := mali400
BOARD_WITH_IOMMU := true
#... note: the following properties are used to load hal modules:
#...    ro.hardware
#...    ro.product.board
#...    ro.board.platform


# rockchip hw/gralloc, hw/hwcomposer
GRAPHIC_MEMORY_PROVIDER := dma_buf
BOARD_USE_LCDC_COMPOSER := true
BOARD_LCDC_COMPOSER_LANDSCAPE_ONLY := false
#...currently this is only used in hwcomposer
TARGET_BOARD_OPTIMIZE := t_station


# framework 
USE_OPENGL_RENDERER := true
#...this may not to be used.
BOARD_USE_LEGACY_UI := true
#. surfaceflinger setting
#... kernel config has CONFIG_[SYNC, SW_SYNC, SW_SYNC_USER].
#TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := true # --> false
#... use sourceCropi instead of sourceCropf of struct hwc_composer_device_1.
HWCOMPOSER_WITH_INTEGER_RECT := true
#... This is also used at hardware/rockchip/libgralloc/Android.mk
#... for test
#NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3 # use default value(2)
#@@@@NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3


# wifi
#...To use realtek driver, it's necessary to add a line:
#...  LIB_WIFI_HAL := libwifi-hal-realtek
#...  in frameworks/opt/net/wifi/service/Android.mk.
#... (Broadcom's bcmdhd driver also works. ie. BOARD_WLAN_DEVICE := bcmdhd .)
BOARD_WLAN_DEVICE := realtek
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
WPA_SUPPLICANT_VERSION := VER_0_8_X
WIFI_DRIVER_SOCKET_IFACE := wlan0
#...this library is build at hardware/<vendor name>/.../wpa_supplicant_8_lib and
#...used to build external/wpa_supplicant_8
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_$(BOARD_WLAN_DEVICE)
#...These macros are used in hardware/libhardware_legacy/wifi/Android.mk
WIFI_DRIVER_STATE_CTRL_PARAM := /sys/class/rkwifi/driver
WIFI_DRIVER_STATE_ON := 1
WIFI_DRIVER_STATE_OFF := 0


# Sepolicy & Seccomp
BOARD_SEPOLICY_DIRS += device/unofficial/t-station/sepolicy
#BOARD_SECCOMP_POLICY += device/unofficial/t-station/seccomp

# root/default_prop
#...No need to set property 'ro.hardware'.
#...Because property 'ro.hardware' is set by the following sequence:
#...  (0) AT BUILD time the <line> in 'ADDITIONAL_DEFAULT_PROPERTIES += <line>'
#...      goes into <root>/default.prop .
#...  (1) LOADER passes the string 'XXX...' of 'CMDLINE:XXX...' in parameter info
#...      which is in the first 4M byte of the nand flash
#...      as a command line parameter.
#...  (2) INIT process parses the command line parameter(/proc/cmdline), and
#...      does setprop ro.boot.XXX=YYY when the pattern 'androidboot.XXX=YYY' is found ,
#...      ( ref. process_kernel_cmdline() in system/core/init/init.cpp )
#...  (3) and does setprop ro.XXX=YYY ,
#...      ( ref. export_kernel_boot_props() in system/core/init/init.cpp )
#,,,  (4) T-station's parameter info has a string
#...      'CMDLINE:... androidboot.hardware=rk30board androidboot.console=ttyFIQ0 ...'.
#...      So property 'ro.hardware' is set to 'rk30board'.
#...  (5) INIT process also does setprop with each line of <root>/default.prop.
#...      ( ref. property_load_boot_defaults in system/core/init/init.cpp )
#...      But property 'ro.hardware' is already set at step(3),
#...      so the line 'ro.hardware=xxx' in <root>/default.prop is ignored.
#ADDITIONAL_DEFAULT_PROPERTIES += ro.hardware=rk30board



# java compile option
#... enable pre-optimization
WITH_DEXPREOPT := true
#... enable position-independent code( save disk space with a slight runtime impact . )
WITH_DEXPREOPT_PIC := true
#... disable pre-opt for the prebuilt module.
#DONT_DEXPREOPT_PREBUILTS
#... pre-opt for only boot image not for apps.
#WITH_DEXPREOPT_BOOT_IMG_ONLY
