##########################
# copy files from $(TARGET_OUT)
##########################
# copy files from $TARGET_OUT
#
# parameters
#   $(1) : files to be copied.
#   $(2) : from where ( ie the files are copied from $(TARGET_OUT)/$(2)/ ).
#   $(3) : to where	  ( ie the files are copied to   $(3)/ ).
#   $(4) : prefix used to avoid conflicts with other rules ( ie LOCAL_MODULE == $(4)<file> ).
#   $(5) : LOCAL_MODULE_CLASS(optional, default==BOOT)
#
# detail
#   for each file
#		  (step1) Setup variables LOCAL_xxx and include $(BUILD_PREBUILT),
#			        to copy
#								from $(LOCAL_PATH)/$(4)<file>
#								to   $(3)/<file>
#     (step2) Define make-rule
#							to create symbolic link
#								from $(LOCAL_PATH)/$(4)<file>
#								to   $(TARGET_OUT)/$(2)/<file>
#
define private_copy_files
$(foreach t,$(1), \
	$(if $(4),,$(error prefix is not specified)) \
	$(eval include $(CLEAR_VARS)) \
	\
	$(step1) \
	$(eval $(if $(5), LOCAL_MODULE_CLASS := $(5), LOCAL_MODULE_CLASS := BOOT)) \
	$(eval LOCAL_MODULE_PATH := $(3) ) \
	$(eval LOCAL_MODULE_TAGS := eng) \
	$(eval LOCAL_MODULE := $(4)$(t)) \
	$(eval LOCAL_MODULE_STEM := $(t)) \
	$(eval LOCAL_SRC_FILES := $(LOCAL_MODULE)) \
	$(eval include $(BUILD_PREBUILT)) \
	\
	$(step2) \
	$(eval $(LOCAL_PATH)/$(LOCAL_MODULE): $(TARGET_OUT)/$(2)/$(LOCAL_MODULE_STEM) ; ln -sf $(CURDIR)/$$< $$@) \
	\
	$(for_debug) \
	$(info LOCAL_MODULE=$(LOCAL_MODULE)) \
	$(info LOCAL_MODULE_STEM=$(LOCAL_MODULE_STEM)) \
	$(info $(LOCAL_PATH)/$(LOCAL_MODULE): $(TARGET_OUT)/$(2)/$(LOCAL_MODULE_STEM) ; ln -sf $(CURDIR)/$$< $$@) \
  )
endef

