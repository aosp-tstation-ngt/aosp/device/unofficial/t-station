#!/sbin/sh


# The first boot will take some time.
/sbin/busybox sleep 600

# get log
/system/bin/logcat -d  -f /data/logcat.log
/sbin/busybox sleep 5

# re-boot into recovey mode.
/system/bin/reboot recovery

