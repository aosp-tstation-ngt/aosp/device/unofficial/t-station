#!/system/bin/sh

# This scripts is generated by executing 'extract.sh' .
# This scripts must be run on the target device.


# Set MNT_POINT where the system partition is mounted.
MNT_POINT=/mnt/system

# check MNT_POINT
if [ ! -f $MNT_POINT/build.prop ] ; then
    echo "error: $MNT_POINT/build.prop does not exist. Please check MNT_POINT"
    exit -1
fi

# check some commands are available
for c in chcon chmod chown
do
    if ! command -v $c > /dev/null ; then
        echo ERROR: $c does not exist.
        exit 1
    fi
done


#----chcon----
echo "INFO: chcon"
#...default context
chcon -hR  u:object_r:system_file:s0  $MNT_POINT
#...other context
chcon  -h  u:object_r:zygote_exec:s0          $MNT_POINT/bin/app_process32
chcon  -h  u:object_r:atrace_exec:s0          $MNT_POINT/bin/atrace
chcon  -h  u:object_r:audioserver_exec:s0     $MNT_POINT/bin/audioserver
chcon  -h  u:object_r:blkid_exec:s0           $MNT_POINT/bin/blkid
chcon  -h  u:object_r:bootanim_exec:s0        $MNT_POINT/bin/bootanimation
chcon  -h  u:object_r:bootstat_exec:s0        $MNT_POINT/bin/bootstat
chcon  -h  u:object_r:cameraserver_exec:s0    $MNT_POINT/bin/cameraserver
chcon  -h  u:object_r:clatd_exec:s0           $MNT_POINT/bin/clatd
chcon  -h  u:object_r:debuggerd_exec:s0       $MNT_POINT/bin/debuggerd
chcon  -h  u:object_r:dex2oat_exec:s0         $MNT_POINT/bin/dex2oat
chcon  -h  u:object_r:dnsmasq_exec:s0         $MNT_POINT/bin/dnsmasq
chcon  -h  u:object_r:drmserver_exec:s0       $MNT_POINT/bin/drmserver
chcon  -h  u:object_r:dumpstate_exec:s0       $MNT_POINT/bin/dumpstate
chcon  -h  u:object_r:fsck_exec:s0            $MNT_POINT/bin/e2fsck
chcon  -h  u:object_r:fsck_exec:s0            $MNT_POINT/bin/fsck.f2fs
chcon  -h  u:object_r:fsck_exec:s0            $MNT_POINT/bin/fsck_msdos
chcon  -h  u:object_r:gatekeeperd_exec:s0     $MNT_POINT/bin/gatekeeperd
chcon  -h  u:object_r:idmap_exec:s0           $MNT_POINT/bin/idmap
chcon  -h  u:object_r:installd_exec:s0        $MNT_POINT/bin/installd
chcon  -h  u:object_r:keystore_exec:s0        $MNT_POINT/bin/keystore
chcon  -h  u:object_r:lmkd_exec:s0            $MNT_POINT/bin/lmkd
chcon  -h  u:object_r:logcat_exec:s0          $MNT_POINT/bin/logcat
chcon  -h  u:object_r:logd_exec:s0            $MNT_POINT/bin/logd
chcon  -h  u:object_r:mdnsd_exec:s0           $MNT_POINT/bin/mdnsd
chcon  -h  u:object_r:mediacodec_exec:s0      $MNT_POINT/bin/mediacodec
chcon  -h  u:object_r:mediadrmserver_exec:s0  $MNT_POINT/bin/mediadrmserver
chcon  -h  u:object_r:mediaextractor_exec:s0  $MNT_POINT/bin/mediaextractor
chcon  -h  u:object_r:mediaserver_exec:s0     $MNT_POINT/bin/mediaserver
chcon  -h  u:object_r:mtp_exec:s0             $MNT_POINT/bin/mtpd
chcon  -h  u:object_r:netd_exec:s0            $MNT_POINT/bin/netd
chcon  -h  u:object_r:dex2oat_exec:s0         $MNT_POINT/bin/patchoat
chcon  -h  u:object_r:ppp_exec:s0             $MNT_POINT/bin/pppd
chcon  -h  u:object_r:profman_exec:s0         $MNT_POINT/bin/profman
chcon  -h  u:object_r:racoon_exec:s0          $MNT_POINT/bin/racoon
chcon  -h  u:object_r:runas_exec:s0           $MNT_POINT/bin/run-as
chcon  -h  u:object_r:sdcardd_exec:s0         $MNT_POINT/bin/sdcard
chcon  -h  u:object_r:servicemanager_exec:s0  $MNT_POINT/bin/servicemanager
chcon  -h  u:object_r:sgdisk_exec:s0          $MNT_POINT/bin/sgdisk
chcon  -h  u:object_r:shell_exec:s0           $MNT_POINT/bin/sh
chcon  -h  u:object_r:surfaceflinger_exec:s0  $MNT_POINT/bin/surfaceflinger
chcon  -h  u:object_r:toolbox_exec:s0         $MNT_POINT/bin/toolbox
chcon  -h  u:object_r:toolbox_exec:s0         $MNT_POINT/bin/toybox
chcon  -h  u:object_r:tzdatacheck_exec:s0     $MNT_POINT/bin/tzdatacheck
chcon  -h  u:object_r:uncrypt_exec:s0         $MNT_POINT/bin/uncrypt
chcon  -h  u:object_r:vdc_exec:s0             $MNT_POINT/bin/vdc
chcon  -h  u:object_r:vold_exec:s0            $MNT_POINT/bin/vold
chcon  -h  u:object_r:wpa_exec:s0             $MNT_POINT/bin/wpa_supplicant
chcon  -h  u:object_r:perfprofd_exec:s0       $MNT_POINT/xbin/perfprofd
chcon  -h  u:object_r:su_exec:s0              $MNT_POINT/xbin/su


#----chmod----
echo "INFO: chmod"
# ...directory
echo "INFO: ... all directory"
find $MNT_POINT -type d -not -name '.' -exec chmod 755 '{}' ';'
# ...bin,xbin
echo "INFO: ... bin xbin"
for dir in bin xbin ; do
    find $MNT_POINT/$dir -type f -not -name '.' -exec chmod 755 '{}' ';'
done
# ...app etc fonts framework lib media priv-app usr
echo "INFO: ... app etc fonts framework lib media priv-app usr"
for dir in app etc fonts framework lib priv-app usr ; do
    find $MNT_POINT/$dir -type f -not -name '.' -exec chmod 644 '{}' ';'
done
# ...other
echo "INFO: ... other"
chmod  o=rw,g=r,o=r     $MNT_POINT/build.prop
chmod  o=rwx,g=rx,o=    $MNT_POINT/bin/run-as
chmod  o=rwx,g=rx,o=    $MNT_POINT/bin/uncrypt
chmod  o=rx,g=rx,o=rx   $MNT_POINT/etc/ppp/ip-up-vpn
chmod  o=rw,g=r,o=r     $MNT_POINT/fake-libs/libart.so
chmod  o=rws,g=rs,o=rx  $MNT_POINT/xbin/procmem
chmod  o=rws,g=rx,o=    $MNT_POINT/xbin/su


#----chown----
echo "INFO: chown"
# ...bin,xbin
echo "INFO: ... bin,xbin"
for dir in bin xbin ; do
    find $MNT_POINT/$dir -type f -not -name '.' -exec chown 0:2000 '{}' ';'
done
#...other
echo "INFO: ... other"
chown  0:2000     $MNT_POINT/bin
chown  0:2000     $MNT_POINT/vendor
chown  0:2000     $MNT_POINT/xbin
chown  1000:1003  $MNT_POINT/bin/surfaceflinger
chown  0:2000     $MNT_POINT/vendor/lib
chown  0:2000     $MNT_POINT/vendor/lib/mediadrm
