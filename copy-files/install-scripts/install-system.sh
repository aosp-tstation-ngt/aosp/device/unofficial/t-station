#!/bin/bash

##############################################################
# (1) Set AOSP_OUT_SYSTEM_DIR to the appropriate directory.
# (2) Make sure 'setprop.sh' is in the same directory as this scripts.

AOSP_DIR=XXXX
AOSP_OUT_SYSTEM_DIR=${AOSP_DIR}/out/target/product/t-station/system
#AOSP_OUT_SYSTEM_DIR= #${AOSP_DIR}/build_output/system
##############################################################



# dir/file name in the device.
SYSTEM_DIR=/mnt/system
SYS_ROM=/dev/block/rknand_system
DATA_ROM=/dev/block/rknand_userdata


# commands
#...PC
SH="adb shell"
SH_B="adb shell busybox"
#...device
BUSYBOX=/sbin/busybox
E2FSCK=/system/bin/e2fsck
MKE2FS=/system/bin/mke2fs
#...both
SETPROP=setprop.sh


# check if AOSP_OUT_SYSTEM_DIR is set.
if [ ! -d $AOSP_OUT_SYSTEM_DIR ] ; then
    echo ERROR: \"$AOSP_OUT_SYSTEM_DIR\" does not exist.
    echo ERROR: please set AOSP_OUT_SYSTEM_DIR to the appropriate directory.
fi
#...check sub dirs.
for f in app bin build.prop etc fake-libs fonts framework lib media priv-app usr vendor xbin
do
    if [ ! -e "$AOSP_OUT_SYSTEM_DIR/$f" ] ; then
        echo ERROR: \"$AOSP_OUT_SYSTEM_DIR/$f\" does not exist.
        echo ERROR: please check $AOSP_OUT_SYSTEM_DIR is correct.
        exit 1
    fi
done


# check if some commands exist.
for c in pushd popd adb grep dirname
do
    if ! command -v $c >/dev/null ; then
        echo ERROR: $c does not exist.
        exit 1
    fi
done
#...adb version check
if adb --help |& grep -q -F '1.0.31' ; then
    echo "ERROR: adb version 1.0.31 can't connect with t-station."
    exit 1
fi
#...check setprop.sh
SETPROP_PC=$(dirname $BASH_SOURCE)/$SETPROP
if [ ! -f ${SETPROP_PC} ] ; then
    echo "ERROR: ${SETPROP_PC} does not exist."
    exit 1
fi


# check device
if ! adb devices | grep -q recovery
then
    echo ERROR: device is not recovery mode !
    exit 1
fi
if ! adb remount
then
    echo ERROR: remount failed !
    exit 1
fi
#...check if some commands exist in target
for c in ${E2FSCK} ${MKE2FS} ${BUSYBOX} chcon chmod chown find mount umount setenforce
do
    if ! ( ${SH} "$c --help" |& grep -q 'Usage:' )
    then
        echo ERROR: $c is not exist in target !
        exit 1
    fi
done


# set permissive
echo INFO: set permissive
if ! ${SH} setenforce 0
then
    echo ERROR: setenforce failed !
    exit 1
fi



# format patition
echo INFO: format partition ${SYS_ROM} and ${DATA_ROM}
echo "

Now formatting the system and data partition"
read -e -i "N" -p "Are you sure[y/N] ?: " ans
if [ "${ans^y}" != "Y" ]
then
    echo INFO: installation is canceled !
    exit 1
fi
${SH} ${MKE2FS} -t ext4 ${SYS_ROM}
${SH} ${MKE2FS} -t ext4 ${DATA_ROM}



# mount partition
echo INFO: mount system partition
#...system
${SH_B} mkdir -p ${SYSTEM_DIR} > /dev/null
${SH_B} mount ${SYS_ROM}  ${SYSTEM_DIR} > /dev/null
if ! ${SH_B} mount | grep -q ${SYS_ROM}
then
    echo ERROR: mount system partition failed !
    exit 1
fi


# copy files
echo INFO: copy files from ${AOSP_OUT_SYSTEM_DIR}
adb push ${AOSP_OUT_SYSTEM_DIR}/* ${SYSTEM_DIR}/



# set property( mode, owner context etc )
adb push ${SETPROP_PC} /tmp
${SH_B} chmod 755 /tmp/${SETPROP}
${SH} /tmp/${SETPROP}



# sync
${SH_B} sync
${SH_B} umount ${SYSTEM_DIR}
echo INFO: done
