o about libGLES_mali.so

  This module is downloaded from rockchip's github.

  download site:
    https://github.com/rockchip-android/vendor-rockchip-common/tree/rk3328-box-7.1/gpu/Mali400/lib/arm

  It seems that
    it's built for Android 7,
    the device driver API version is 900 ( which must match the mali.ko's API version ),
    it requires LIBC_N version of __eabi_xxx() in bionic/libc,
    is uses GNU_HASH ( ie. linked with '-Wl,--hash-style=gnu' option ).


o Some problems in Android 6 ( There is no problem in Android 7 )

  Loading this module in Android 6 causes error as below:
  	E libEGL  : load_driver(/system/lib/egl/libGLES_mali.so): dlopen failed: cannot locate symbol "__aeabi_memclr8" referenced by "/system/lib/egl/libGLES_mali.so"...
		F libEGL  : couldn't find an OpenGL ES implementation

  To prevent the error, adding LIBC_N version of __eabi_xxx in bionic/libc is required.

