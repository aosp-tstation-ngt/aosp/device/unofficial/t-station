This directory contains rochchip's tools to read/write nand flash memory:

 * androidtool & driver assistant
    AndroidTool_Release_v2.52.zip
    DriverAssitant_v4.6.zip

    download site:
		    http://opensource.rock-chips.com/wiki_AndroidTool
		    https://github.com/rockchip-linux/tools/tree/rk3399/windows


 * linux upgrade tool
    Linux_Upgrade_Tool_1.34.zip

		download site: http://www.t-firefly.com/doc/download/31.html#linux_12

 * boot loader
   rk3126_loader_v2.09.257.bin'

   This is built with the following steps:
     1. clone Rockchip's rkbin repository, and check out.

        site: https://github.com/rockchip-linux/rkbin.git
        branch: master
        commit: 0bb1c512492386a72a3a0b5a0e18e49c636577b9 or
                80e23b0da6fd41c2ca35693e58264d5a59360fe6

     2. execute boot_merger as below:
        ./tools/boot_merger --pack ./RKBOOT/RK3126MINIALL.ini


 * upgrade_tool from Rockchip's rkbin repository
   upgrade_tool-143 (version 1.43)
   upgrade_tool-154 (version 1.54)

   These files are from Rockchip's rkbin repository:
     site: https://github.com/rockchip-linux/rkbin.git
     branch: master

     commit: 0bb1c512492386a72a3a0b5a0e18e49c636577b9 ( version 1.54 )
     commit: 0bb1c512492386a72a3a0b5a0e18e49c636577b9 ( version 1.43 )


   note: Version 1.54 has some glitches as below:
     DI(Download Image) command works only when it is specified in command line parameter:

       upgrade_tool-154  DI -b boot.img <enter>

     It does not work when specified in upgrade_tool's command prompt:

       upgrade_tool-154 <enter>
       Rockusb>DI -b boot.img <enter>

     And shows an error message:

       command is invalid,please check help!
