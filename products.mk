# Copyright (C) 2013 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


##########################
# packages
##########################
# from generic_no_telephony.mk
PRODUCT_PACKAGES += \
    Bluetooth \
    OneTimeInitializer \
    Provision \
    SystemUI \
    audio.primary.default \
    audio_policy.default \
    clatd \
    clatd.conf \
    librs_jni \
    libvideoeditor_core \
    libvideoeditor_jni \
    libvideoeditor_osal \
    libvideoeditor_videofilters \
    libvideoeditorplayer \
    local_time.default \
    power.default \
    pppd \
    screenrecord \
    vibrator.default
# removed
#    BluetoothMidiService
#    Camera2
#    EasterEgg
#    Gallery2
#    Music
#    MusicFX
#    WallpaperCropper


# from core.mk
PRODUCT_PACKAGES += \
    BookmarkProvider \
    CaptivePortalLogin \
    CertInstaller \
    Contacts \
    DocumentsUI \
    DownloadProviderUi \
    ExternalStorageProvider \
    FusedLocation \
    InputDevices \
    KeyChain \
    Keyguard \
    LatinIME \
    Launcher2 \
    MtpDocumentsProvider \
    PacProcessor \
    libpac \
    ProxyHandler \
    Settings \
    SharedStorageBackup \
    StorageManager

# removed
#    BasicDreams \
#    Browser \
#    Calculator \
#    Calendar \
#    CalendarProvider \
#    DeskClock \
#    Email \
#    Exchange2 \
#    ManagedProvisioning \
#    PicoTts \
#    PrintSpooler \
#    QuickSearchBox \
#    Telecom \
#    TeleService \
#    VpnDialogs \
#    MmsService

# Use Launcher2 instead of Launcher3,
#   because a handle of the drawer, which contains all installed apps,
#   does not show in landscape mode of Launcher3.
#   ( It shows in portrait mode.
#     But T-station has no gravity sensor,
#     so we can not rotate the screen by tilting the device.
#   )



PRODUCT_PACKAGES += \
    mke2fs \
    OpenWnn \
    adb \
    audio.a2dp.default \
    audio.r_submix.default \
    audio.usb.default \
    wpa_supplicant \
    wpa_supplicant.conf


# hal
PRODUCT_PACKAGES += \
    power.rk312x \
    audio.primary.rk312x \
    gralloc.rk312x \
    hwcomposer.rk312x \
    lights.rk312x


# document
PRODUCT_PACKAGES += \
    online-sac \
		online-sac-docs


# root and recovery image
PRODUCT_PACKAGES += \
    recovery \
    recovery_extra_for_tstation \
    root_extra_for_tstation


# for debug
PRODUCT_PACKAGES += \
    tinypcminfo \
    tinymix \
    tinyplay \
    bssl \
    iontest \
    amix


# not used
#    libemoji
#    sound_trigger.stub.default
#    org.apache.http.legacy
#    com.android.contacts.common
#    audio.alsa_usb.rk312x
#    audio_policy.rk312x
#    libaudiopolicy_rk312x
#    online-sac-docs

# automatically included(required)
#    libtinyalsa
#    libion



# kernel and resource
PRODUCT_PACKAGES += \
    tstation-kernel \
    tstation-resource \
    tstation-uboot


##########################
# copy files
##########################

# media
PRODUCT_COPY_FILES += \
    frameworks/av/media/libeffects/data/audio_effects.conf:system/etc/audio_effects.conf \
    frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:system/etc/media_codecs_google_audio.xml \
    frameworks/av/media/libstagefright/data/media_codecs_google_video_le.xml:system/etc/media_codecs_google_video_le.xml \
    $(LOCAL_PATH)/copy-files/etc/audio_policy.conf:system/etc/audio_policy.conf \
    $(LOCAL_PATH)/copy-files/etc/media_codecs.xml:system/etc/media_codecs.xml \
    $(LOCAL_PATH)/copy-files/etc/media_profiles.xml:system/etc/media_profiles.xml


# permission
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
    frameworks/native/data/etc/android.hardware.faketouch.xml:system/etc/permissions/android.hardware.faketouch.xml \
    frameworks/native/data/etc/android.hardware.location.xml:system/etc/permissions/android.hardware.location.xml \
    $(LOCAL_PATH)/copy-files/etc/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml


# wifi
# rtl8189fs driver gets efuse file from:
#    kernel-3.10.104 /system/etc/firmware/wifi_efuse_8189fs.map
#    kernel-4.4      /system/etc/wifi/wifi_efuse_8189fs.map
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/copy-files/etc/wifi_efuse_8189fs.map:system/etc/wifi/wifi_efuse_8189fs.map
#    $(LOCAL_PATH)/copy-files/etc/wifi_efuse_8189fs.map:system/etc/firmware/wifi_efuse_8189fs.map


#PRODUCT_COPY_FILES += \
#    prebuilts/sdk/org.apache.http.legacy/org.apache.http.legacy.jar:system/framework/org.apache.http.legacy.jar

# root directory
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/copy-files/root/fstab.rk30board.bootmode.unknown:root/fstab.rk30board.bootmode.unknown \
    $(LOCAL_PATH)/copy-files/root/init.connectivity.rc:root/init.connectivity.rc \
    $(LOCAL_PATH)/copy-files/root/init.recovery.rk30board.rc:root/init.recovery.rk30board.rc \
    $(LOCAL_PATH)/copy-files/root/init.rk30board.bootmode.unknown.rc:root/init.rk30board.bootmode.unknown.rc \
    $(LOCAL_PATH)/copy-files/root/init.rk30board.environment.rc:root/init.rk30board.environment.rc \
    $(LOCAL_PATH)/copy-files/root/init.rk30board.rc:root/init.rk30board.rc \
    $(LOCAL_PATH)/copy-files/root/init.rk30board.usb.rc:root/init.rk30board.usb.rc \
    $(LOCAL_PATH)/copy-files/root/ueventd.rk30board.rc:root/ueventd.rk30board.rc

#    $(LOCAL_PATH)/copy-files/root/fstab.rk30board.bootmode.emmc:root/fstab.rk30board.bootmode.emmc
#    $(LOCAL_PATH)/copy-files/root/init.rk30board.bootmode.emmc.rc:root/init.rk30board.bootmode.emmc.rc

# moved to device/unofficial/t-station-binary
# # kernel
# PRODUCT_COPY_FILES += \
#     $(LOCAL_PATH)/copy-files/kernel/zImage:kernel


# install scripts
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/copy-files/install-scripts/extract.sh:install-scripts/extract.sh \
    $(LOCAL_PATH)/copy-files/install-scripts/setprop.sh:install-scripts/setprop.sh \
    $(LOCAL_PATH)/copy-files/install-scripts/install-system.sh:install-scripts/install-system.sh


# install tools
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/copy-files/install-tools/AndroidTool_Release_v2.52.zip:install-tools/AndroidTool_Release_v2.52.zip  \
    $(LOCAL_PATH)/copy-files/install-tools/DriverAssitant_v4.6.zip:install-tools/DriverAssitant_v4.6.zip        \
    $(LOCAL_PATH)/copy-files/install-tools/Linux_Upgrade_Tool_1.34.zip:install-tools/Linux_Upgrade_Tool_1.34.zip    \
		$(LOCAL_PATH)/copy-files/install-tools/rk3126_loader_v2.09.257.bin:install-tools/rk3126_loader_v2.09.257.bin \
		$(LOCAL_PATH)/copy-files/install-tools/upgrade_tool-143:install-tools/upgrade_tool-143 \
		$(LOCAL_PATH)/copy-files/install-tools/upgrade_tool-154:install-tools/upgrade_tool-154 \
    $(LOCAL_PATH)/copy-files/install-tools/readme.txt:install-tools/readme.txt


# egl
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/copy-files/system/lib/egl/egl.cfg:system/lib/egl/egl.cfg \
    $(LOCAL_PATH)/copy-files/system/lib/egl/libGLES_mali.so:system/lib/egl/libGLES_mali.so


# for debug
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/copy-files/root/recov.sh:root/sbin/recov.sh


##########################
# property overrides
##########################
# "PRODUCT_PROPERTY_OVERRIDES += xxx" are moved to ./system.prop

# default.prop in root ramfs
PRODUCT_DEFAULT_PROPERTY_OVERRIDES := \
    ro.config.low_ram=true \
    ro.sf.lcd_density=160 \
    ro.adb.secure=0


##########################
# inherit-product
##########################
$(call inherit-product, build/target/product/core_base.mk)
$(call inherit-product-if-exists, frameworks/webview/chromium/chromium.mk)
$(call inherit-product-if-exists, frameworks/base/data/keyboards/keyboards.mk)
$(call inherit-product-if-exists, frameworks/base/data/fonts/fonts.mk)
$(call inherit-product-if-exists, external/roboto-fonts/fonts.mk)
$(call inherit-product-if-exists, frameworks/base/data/sounds/AudioPackage5.mk)

# 0525 # 1014
# 0525 $(call inherit-product-if-exists, frameworks/native/build/tablet-7in-hdpi-1024-dalvik-heap.mk)
$(call inherit-product-if-exists, frameworks/native/build/tablet-dalvik-heap.mk)

# 1016
$(call inherit-product-if-exists, external/noto-fonts/fonts.mk)
$(call inherit-product-if-exists, external/hyphenation-patterns/patterns.mk)
