This is a T-Station specific project.

T-Station Spec:

  type : tablet
  board : INCAR AGZ-RK3126-V1.0 2016-07-12 ( 04C0550236A0 )
  cpu : Rockchip RK3126
  memory : 1G bytes ( SK hynix H5TC4G63AFR * 2 )
  nand flash : 4G bytes ( SanDisk X61143891 SDTNQFAMA-004G * 1 )
  wifi : REALTEK 8189FTV G5A60C4
  power : AXP192
  lcd : 1024 x 600 IPS, 5 point touch screen
  touch screen : Silead GSL1680
  real time clock : VOLCOS VC8563
  op-amp : NS4150B * 2

  phone : none
  bluetooth : none
  battery : none
  nfc : none
  sensor : none
  camera : none
  
