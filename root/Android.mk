ifneq ($(filter t-station,$(TARGET_DEVICE)),)
LOCAL_PATH := $(call my-dir)


# Initialize some variables with null string
# to prevent deffered evaluation of the right hand side of '+=' operator.
# According to the GNU Make manual:
#   For the append operator '+=', the right-hand side is considered immediate
#   if the variable was previously set as a simple variable (‘:=’ or ‘::=’),
#   and deferred otherwise.
# ( LOCAL_xxx are cleared by include $(CLEAR_VARS) )
TMP_DEPS :=
TMP_EXECUTABLES :=
TMP_SHARED_LIBS :=


##########################
# root_extra_for_tstation
##########################
include $(CLEAR_VARS)
TMP_DEPS :=
LOCAL_MODULE := root_extra_for_tstation



# copy busybox and
# make symlink /sbin/sh -> /sbin/busybox
LOCAL_ADDITIONAL_DEPENDENCIES += busybox-aosp
LOCAL_POST_INSTALL_CMD += \
	mkdir -p $(TARGET_ROOT_OUT)/sbin ; \
	cp -f $(PRODUCT_OUT)/utilities/busybox-aosp $(TARGET_ROOT_OUT)/sbin/busybox ; \
	ln -sf /sbin/busybox $(TARGET_ROOT_OUT)/sbin/sh ;
TMP_DEPS += \
	$(TARGET_ROOT_OUT)/sbin/busybox \
	$(TARGET_ROOT_OUT)/sbin/sh



# copy bin/xxx and required libs to root
# This is for debugging boot image( without starting zygote ).
TMP_EXECUTABLES += sh linker reboot
TMP_SHARED_LIBS += \
		libc++.so \
		libc.so \
		libdl.so \
		libm.so
# TMP_EXECUTABLES += toolbox toybox 
# TMP_SHARED_LIBS += \
# 		libcrypto.so \
# 		libcutils.so \
# 		liblog.so \
# 		libpackagelistparser.so \
# 		libpcre.so \
# 		libselinux.so \
# 		libstdc++.so
# TMP_EXECUTABLES += e2fsck mke2fs
# TMP_SHARED_LIBS += \
# 		libext2_blkid.so \
# 		libext2_com_err.so \
# 		libext2_e2p.so \
# 		libext2_profile.so \
# 		libext2_quota.so \
# 		libext2_uuid.so \
# 		libext2fs.so
LOCAL_REQUIRED_MODULES := $(TMP_EXECUTABLES) $(TMP_SHARED_LIBS)
#- include $(LOCAL_PATH)/../copy-files.mk
#- $(call private_copy_files,$(TMP_EXECUTABLES),bin,$(TARGET_ROOT_OUT)/system/bin,root_)
#- $(call private_copy_files,$(TMP_SHARED_LIBS),lib,$(TARGET_ROOT_OUT)/system/lib,root_)
LOCAL_POST_INSTALL_CMD += \
	mkdir -p $(TARGET_ROOT_OUT)/system/bin ; \
	mkdir -p $(TARGET_ROOT_OUT)/system/lib ; \
	for f in $(TMP_EXECUTABLES) ; do cp $(TARGET_OUT)/bin/$$f $(TARGET_ROOT_OUT)/system/bin/ ; done ; \
	for f in $(TMP_SHARED_LIBS) ; do cp $(TARGET_OUT)/lib/$$f $(TARGET_ROOT_OUT)/system/lib/ ; done ;
TMP_DEPS += $(patsubst %,$(TARGET_ROOT_OUT)/system/bin/%,$(TMP_EXECUTABLES))
TMP_DEPS += $(patsubst %,$(TARGET_ROOT_OUT)/system/lib/%,$(TMP_SHARED_LIBS))
# copy bin/xxx and required libs to root




# symlink fstab.rk30board -> fstab.rk30board.bootmode.unknown 
LOCAL_POST_INSTALL_CMD += \
	mkdir -p $(TARGET_ROOT_OUT) ; \
	ln -sf /fstab.rk30board.bootmode.unknown $(TARGET_ROOT_OUT)/fstab.rk30board ;
TMP_DEPS += \
	$(TARGET_ROOT_OUT)/fstab.rk30board.bootmode.unknown \
	$(TARGET_ROOT_OUT)/fstab.rk30board



# LOCAL_POST_INSTALL_CMD is executed when
#   (1) Android.mk or
#   (2) LOCAL_ADDITIONAL_DEPENDENCIES
#		is updated.
#
# So
# update LOCAL_ADDITIONAL_DEPENDENCIES( == ./timestamp )
# when the target files are not exist ( maybe manually deleted ).
TIMESTAMP := $(LOCAL_PATH)/timestamp
TMP_DEPS += $(TIMESTAMP)
DUMMY := $(shell for f in $(TMP_DEPS) ; do [ ! -e $$f ] && [ ! -L $$f ] && echo $$f > $(TIMESTAMP) && break ; done)


LOCAL_ADDITIONAL_DEPENDENCIES += $(TIMESTAMP)
include $(BUILD_PHONY_PACKAGE)
##########################

endif
