/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// linux
#include <linux/fb.h>
#include <sys/mman.h>

// std-c
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

// c++
#include <functional>
#include <thread>
#include <string>

// android
#include <graphics.h>
#include <cutils/properties.h>

#include "device.h"
#include "screen_ui.h"
#include "minui/minui.h"  // for gr_xxx

//**************************************
// configulation
//**************************************
#define NUM_BUFFERS 2

// specify how to push key event
//  defined( TST_USE_ENQUEUE_KEY) : by calling RecoveryUI::EnqueueKey
// !defined( TST_USE_ENQUEUE_KEY) : by writing keycodes directry into /dev/input/eventXX
#define  TST_USE_ENQUEUE_KEY

#define TST_HELP_TEXT   "Use swipe-up/down and power-button."

//**************************************
// debug
//**************************************
#define TST_ERR(msg...) fprintf(stderr, "ERROR:" msg)
#define TST_DBG(msg...) fprintf(stderr, msg)

// CALL macro requires that 'int line' is defined.
#ifdef CALL
#error macro CALL is defined elsewhere.
#endif
#define CALL(fnc,...) (line=__LINE__, fnc(__VA_ARGS__))

#define TST_UNUSED    __attribute__((unused))

//**************************************
// constant
//**************************************
// frame buffer device
static char const*const  fb_dvices[] = 	{
		"/dev/graphics/fb0",
		"/dev/fb0",
};

#ifndef STR
#define STR(s)    #s
#endif

// event device
#define   EVENT_DEVICE_NAME "/dev/input/event"
//...max number in /dev/input/event<number>
#define   MAX_DEVICE_NUMBER 10
#define   MAX_DEVICE_NUMBER_DIGITS sizeof(STR(MAX_DEVICE_NUMBER))


#define TEXT_INDENT     4


//**************************************
// utils
//**************************************
#ifndef ARRAY_SIZE
#define ARRAY_SIZE(A) ((int)(sizeof(A)/sizeof(*(A))))
#endif

#define BITS_PER_LONG (sizeof(unsigned long) * 8)
#define BITS_TO_LONGS(x) (((x) + BITS_PER_LONG - 1) / BITS_PER_LONG)

#ifndef ALLIGN
#define ALIGN(x,a) ALIGN_SUB(x, (typeof(x)) (a) - 1)
#define ALIGN_SUB(x,mask) (((x) + (mask)) & ~(mask))
#endif



//--------------------------------------
// frame buffer
//--------------------------------------
void  print_info( struct fb_var_screeninfo&info, struct fb_fix_screeninfo&finfo )
{
    printf("(%d %d) v(%d %d) ofs(%d %d) bits(%d) gr(x%x) rgba(%d:%d %d:%d %d:%d %d:%d)",
           info.xres,
           info.yres,
           info.xres_virtual,
           info.yres_virtual,
           info.xoffset,
           info.yoffset,
           info.bits_per_pixel,
           info.grayscale,
           info.red.offset,
           info.red.length,
           info.green.offset,
           info.green.length,
           info.blue.offset,
           info.blue.length,
           info.transp.offset,
           info.transp.length
        );
    printf(" -- slen(%d) line(%d) cap(x%x)\n",
           finfo.smem_len,
           finfo.line_length,
           finfo.capabilities
        );
}


int clear_frame_buffer( struct fb_var_screeninfo&info, struct fb_fix_screeninfo&finfo, int fd )
{
    size_t  fbSize = ALIGN(finfo.line_length * info.yres_virtual, PAGE_SIZE);
    void*   vaddr = mmap(0, fbSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    int     rc = 0;

    if ( vaddr == MAP_FAILED ) {
        TST_ERR("mmap rc:%d(%s)", errno, strerror(errno));
        rc = - errno;
        goto ret;
    }
    memset(vaddr, 0, fbSize);
    munmap(vaddr, fbSize);

  ret:
    return rc;
}


//--------------------------------------
// event
//--------------------------------------
static char const*   event_name( int type )
{
    #define EV_ENTRY(ev)  name[EV_##ev] = #ev
    static char const*   name[EV_MAX];
    static bool   initialized = false;
    if ( !initialized ) {
        initialized = true;
        memset(name, 0, sizeof(name));
        EV_ENTRY(SYN);
        EV_ENTRY(KEY);
        EV_ENTRY(REL);
        EV_ENTRY(ABS);
        EV_ENTRY(MSC);
        EV_ENTRY(LED);
        EV_ENTRY(SND);
        EV_ENTRY(REP);
        EV_ENTRY(FF);
        EV_ENTRY(FF_STATUS);
        EV_ENTRY(PWR);
    }
    return name[type] ?: "unknown";
}

static int  test_bit(size_t bit, unsigned long* array) {
    return (array[bit/BITS_PER_LONG] & (1UL << (bit % BITS_PER_LONG))) != 0;
}

static void print_event(struct input_event* ev)
{
    char const*   name = event_name(ev->type);
    printf("time:%ld.%ld type:%d(%s) code:%d(x%x) value:%d\n",
           ev->time.tv_sec, ev->time.tv_usec,
           ev->type, name, ev->code, ev->code, ev->value );
}

/*
  find a device name which satisfy the requirements.
  return 
    device name(char*): string "/dev/input/eventXXX" which matched the requirement.
    ==0  : error
*/
static char* scan_dev_event( char const* name_req,
                      int ev_bits_req[],  // required event type,  [last] == -1
                      int cd_bits_req[],  // required code, [last] == -1
                      int cd_type,        // required code type, ex EV_KEY,EV_ABS...
                      int cd_bits_len     // 
    )
{
    int                 rc = 0;
    int                 line = 0;
    char*               found = 0;
    static char         dev_name[sizeof(EVENT_DEVICE_NAME) + MAX_DEVICE_NUMBER_DIGITS + 1];

    TST_DBG("scan_dev_event: name:%s\n", name_req ?: "not specified");

    if ( ! cd_bits_req ) {
        cd_type = EV_KEY;
        cd_bits_len = KEY_MAX;
    }
    for ( int devno = 0 ; devno <= MAX_DEVICE_NUMBER ; devno ++ ) {
        int             fd;
        unsigned long   ev_bits[BITS_TO_LONGS(EV_CNT)];
        unsigned long   cd_bits[BITS_TO_LONGS(cd_bits_len)];
        struct input_id device_info;
        char            name[64] = "Unknown";
        int             version;
        
        snprintf(dev_name, sizeof(dev_name), EVENT_DEVICE_NAME "%d", devno);
        memset(ev_bits, 0, sizeof(ev_bits));
        memset(cd_bits, 0, sizeof(cd_bits));
        if ( ( fd = CALL(open, dev_name, O_RDONLY)) < 0 ||
             CALL(ioctl, fd, EVIOCGVERSION, &version) == -1 ||
             CALL(ioctl, fd, EVIOCGID, &device_info) == -1 ||
             CALL(ioctl, fd, EVIOCGNAME(sizeof(name)), name) == -1 ||
             CALL(ioctl, fd, EVIOCGBIT(0, EV_MAX), ev_bits) == -1 ||
             CALL(ioctl, fd, EVIOCGBIT(cd_type, cd_bits_len), cd_bits) == -1 
            ) {
            rc = -errno;
            if ( fd >= 0 )
                close(fd);
            goto ret;
        }
        close(fd);
        TST_DBG("------------\n");
        TST_DBG("name=%s\n", name);
        TST_DBG("vendor %04hx product %04hx version %04hx\n", device_info.vendor, device_info.product, device_info.version);
        TST_DBG("Supported event types:\n");
        for ( int ev = 0; ev < EV_CNT ; ev ++) {
            if ( test_bit(ev, ev_bits) ) {
                TST_DBG("  event %d(%s)\n", ev, event_name(ev));
            }
        }

        // supprted code( for debug )
        TST_DBG("Supported codes(%d:%s)\n", cd_type, event_name(cd_type));
        for ( int code = 0; code < cd_bits_len ; code++ ) {
            if ( test_bit(code, cd_bits) ) {
                TST_DBG("  code:%d(x%x)\n", code, code);
            }
        }

        // check if name_req is satisfied
        if ( name_req && ! strstr(name, name_req) ) {
            TST_DBG("name(%s) not found. skipping\n", name_req);
            continue; // not satisfied
        }

        // check if ev_bits_req is satisfied
        if ( ev_bits_req ) {
            int   i;
            for ( i = 0 ; ev_bits_req[i] >= 0 ; i ++ ) {
                if ( !test_bit(ev_bits_req[i], ev_bits) ) {
                    TST_DBG("event %d(%s) not supported\n", ev_bits_req[i], event_name(ev_bits_req[i]));
                    break;
                }
            }
            if ( ev_bits_req[i] >= 0 )
                continue; // not satisfied
        }

        // check if cd_bits_req is satisfied
        if ( cd_bits_req ) {
            int   i;
            for ( i = 0 ; cd_bits_req[i] >= 0 ; i ++ ) {
                if ( !test_bit(cd_bits_req[i], cd_bits) ) {
                    TST_DBG("code %d not supprted\n", cd_bits_req[i]);
                    break;
                }
            }
            if ( cd_bits_req[i] >= 0 )
                continue; // not satisfied
        }            

        // found
        TST_DBG("scan_dev_event: found(%s)\n", dev_name);
        found = dev_name;
        break;
    }

  ret:
    if ( rc ) {
        TST_ERR("scan_dev_event: not found rc:%d(%s) line:%d\n", rc, strerror(errno), line);
    }
    return found;
}

/*
  read swipe event from fdTP
  push key event by calling push_event_fnc
 */
typedef std::function<void(int key)>  PUSH_EVENT_FNC;
static void   convert_swipe( int fdTP, PUSH_EVENT_FNC push_event_fnc)
{
    int const not_defined = -2;
    int       slot_curr = not_defined; // -2:unknown
    int       slot_tracking;
    int       x_start, y_start, x_last, y_last;
    int       is_tracking = false;
          
    // prevent un-initialized warning
    slot_tracking = x_start = y_start = x_last = y_last = 0;

    // convert swipe up/down --> key KEY_VOLUMEUP(115) / KEY_VOLUMEDOWN(114)
    /*
      state               is_       slot_   slot_     event
                          tracking  curr    tracking  
      ---------------------------------------------------------------------------
      slot==?
        any state         x         ?       x         SLOT                   --> slot>=0 : not tracking
        initial           false     ?       x         TRACKING_ID.value>=0   --> slot==? : start tracking
        not tracking      false     ?       x         TRACKING_ID.value>=0   --> slot==? : start tracking
        start tracking    true      ?       <--curr   no event               --> slot==? : tracking
        tracking
          id matched      true      ?       ==curr    TRACKING_ID.value<0    --> slot==? : not tracking
          id not matched  true      ?       !=curr    ignore

      slot>=0
        not tracking      false     >=0     x         TRACKING_ID.value>=0   --> slot>=0 : start tracking
        start tracking    true      >=0     <--curr   no event               --> slot>=0 : tracking
        tracking
          id matched      true      >=0     ==curr    TRACKING_ID.value<0    --> slot>=0 : not tracking
          id not matched  true      >=0     !=curr    ignore

      ?: not defined
      x: any value
      <--: set value
      -->: transit to new state

    */
    while (1) {
        struct input_event  event;
        int   rd = read(fdTP, &event, sizeof(event));
        if ( rd < (int)sizeof(event) ) {
            TST_ERR("reading event\n");
            continue;
        }
        //print_event(&event);

        if ( event.type != EV_ABS ) {
            continue;
        }

        switch (event.code) {
        case ABS_MT_SLOT:
            if ( slot_curr == not_defined ) {
                // This is the first ABS_MT_SLOT event.
                // reset to initial state
                is_tracking = false;
            }
            slot_curr = event.value;
            break;
        case ABS_MT_TRACKING_ID:
            if ( is_tracking ) {
                if ( slot_tracking == slot_curr && event.value < 0 ) {
                    // push key event
                    if ( x_start >= 0 &&
                         y_start >= 0 &&
                         abs(x_last - x_start) < 100 &&
                         abs(y_last - y_start) > 30 
                        ) {

                        // for test
                        //push_key_event(fdKey, y_start < y_last ? KEY_VOLUMEUP : KEY_VOLUMEDOWN);
                        push_event_fnc( y_start < y_last ? KEY_VOLUMEUP : KEY_VOLUMEDOWN );
                    }
                    // reset
                    is_tracking = false;
                }
            } else {
                // ! is_tracking
                if ( event.value >= 0 ) {
                    // start tracking
                    is_tracking = true;
                    slot_tracking = slot_curr;
                    x_start = y_start = not_defined;
                }
            }
            break;
        case ABS_MT_POSITION_X:
            if ( !is_tracking || slot_curr != slot_tracking )
                break;
            x_last = event.value;
            if ( x_start == not_defined )
                x_start = x_last;
            break;
        case ABS_MT_POSITION_Y:
            if ( !is_tracking || slot_curr != slot_tracking )
                break;
            y_last = event.value;
            if ( y_start == not_defined )
                y_start = y_last;
            break;
        default:;
        }
    }
}



//**************************************
// TStationUI
//**************************************
class TStationUI : public ScreenRecoveryUI {
public:
    TStationUI() : fdTP_(-1), bShowingFile(false) {};
    ~TStationUI() { if ( fdTP_ >= 0 ) close(fdTP_); };

    void Init() override {
        SetupDisplayMode();
        StartInputLoop();
        ScreenRecoveryUI::Init();
    }

    // override RecoveryUI::WaitKey
    virtual int WaitKey();


protected:
    // override ScreenRecoveryUI::ShowFile
    virtual void ShowFile(FILE*);

    void draw_screen_locked() override;
    
private:
    void SetupDisplayMode();
    void StartInputLoop();

    int         fdTP_;    // fd for the touch pannel
    std::thread input_thread_;
    bool        bShowingFile; // true if in ShowFile(), be updated in ShowFile()
};

Device* make_device() {
    return new Device(new TStationUI);
}

//--------------------------------------
// public
//--------------------------------------
// override RecoveryUI::WaitKey
/*
  Reverse the meaning of swipe up/down when processing ShowFile().
  Because, when viewing log files, 
  it is not inututive that swipe_up == scroll_up and swipe_down == scroll_down.
*/
int TStationUI::WaitKey()
{
    int     key = RecoveryUI::WaitKey();
    if ( ! bShowingFile )
        goto ret;

    switch ( key ) {
    case KEY_UP:
    case KEY_VOLUMEUP:
        key = KEY_DOWN;
        break;

    case KEY_DOWN:
    case KEY_VOLUMEDOWN:
        key = KEY_UP;
        break;
    }
  ret:
    return key;
}

//--------------------------------------
// protected
//--------------------------------------
// override ScreenRecoveryUI::ShowFile
void TStationUI::ShowFile(FILE* f)
{
    bShowingFile = true;
    ScreenRecoveryUI::ShowFile(f);
    bShowingFile = false;
}


/*
  Over write 
    default help text("Use volume up/down and power.")
    with TST_HELP_TEXT.
 */
void TStationUI::draw_screen_locked()
{
    // fingerprint_lines <--
    static int    fingerprint_lines = -1;
    if ( fingerprint_lines < 0 ) {
        char recovery_fingerprint[PROPERTY_VALUE_MAX];
        property_get("ro.bootimage.build.fingerprint", recovery_fingerprint, "");
        std::string  str(recovery_fingerprint);
        fingerprint_lines = str.size() ? 1 + std::count(str.begin(), str.end(), ':') : 0;
    }
    
    ScreenRecoveryUI::draw_screen_locked();
    
    if ( show_text && show_menu ) {
        int y = ( char_height_ + 4 ) * ( fingerprint_lines + 1 ); // +1 == line of "Android Recovery"

        // clear
        gr_color(0, 0, 0, 255);
        gr_fill(0, y, gr_fb_width(), y + char_height_ );

        // write
        SetColor(INFO);
        DrawTextLine(TEXT_INDENT, &y, TST_HELP_TEXT, false);
    }
}


//--------------------------------------
// private
//--------------------------------------
void  TStationUI::SetupDisplayMode()
{
    struct fb_fix_screeninfo  finfo;
    struct fb_var_screeninfo  info;
    int                       rc = 0, line = 0;
    
    // fd <--
    int fd = -1;
    for ( int i = 0 ; fd < 0 && i < ARRAY_SIZE(fb_dvices) ; i ++ ) {
        fd = CALL(open, fb_dvices[i], O_RDWR, 0);
    }

    // for test
    //if (ioctl(fd, FBIOBLANK, FB_BLANK_POWERDOWN) == -1) {
    //    TST_ERR("unable to blank display: %s\n", strerror(errno));
    //    rc = -errno;
    //    goto ret;
    //}

    if ( fd < 0 ||
         CALL(ioctl, fd, FBIOGET_FSCREENINFO, &finfo ) == -1 ||
         CALL(ioctl, fd, FBIOGET_VSCREENINFO, &info) == -1 ||
         CALL(clear_frame_buffer, info, finfo, fd) < 0 ) {
        rc = -errno;
        goto ret;
    }
    TST_DBG("smem_len=%d\n", finfo.smem_len);
    print_info(info, finfo);


    info.reserved[0] = 0;
    info.reserved[1] = 0;
    info.reserved[2] = 0;
    //@@@@info.reserved[3] = 1;
    info.reserved[3] = 0;
    info.xoffset = 0;
    info.yoffset = 0;
    info.activate = FB_ACTIVATE_NOW;

    // only HAL_PIXEL_FORMAT_BGRA_8888 is supported
    info.bits_per_pixel = 32;
    info.red.offset     = 16;
    info.red.length     = 8;
    info.green.offset   = 8;
    info.green.length   = 8;
    info.blue.offset    = 0;
    info.blue.length    = 8;
    info.transp.offset  = 0;
    info.transp.length  = 0;

    info.grayscale      &= 0xff;
    info.grayscale      |= (info.xres<<8) + (info.yres<<20);
    //info.grayscale      |= RK_FB_CFG_DONE_FLAG;
    info.nonstd &= 0xffffff00;
    info.nonstd |= HAL_PIXEL_FORMAT_BGRA_8888;

    // double bufferring
    info.yres_virtual = info.yres * NUM_BUFFERS;
    if (ioctl(fd, FBIOPUT_VSCREENINFO, &info) == -1) 	{
        info.yres_virtual = info.yres;
        TST_ERR("FBIOPUT_VSCREENINFO failed, page flipping not supported fd: %d", fd);
        rc = -errno;
        goto ret;
    }
    print_info(info, finfo);

    // for debug
    if ( fd < 0 ||
         CALL(ioctl, fd, FBIOGET_FSCREENINFO, &finfo ) == -1 ||
         CALL(ioctl, fd, FBIOGET_VSCREENINFO, &info) == -1 ) {
        rc = -errno;
        goto ret;
    }
    TST_DBG("smem_len=%d\n", finfo.smem_len);
    print_info(info, finfo);

  ret:
    if ( rc ) {
        TST_ERR("SetupDisplayMode: line:%d\n", line );
    }
    if ( fd >= 0 )
        close(fd);
    ;
}

void TStationUI::StartInputLoop()
{
    int                 fdTP = -1;  // fd for touch pannel
    int                 rc = 0;
    int                 line = 0;
    char*               devname = 0;
    //...requirement for touch pannel
    char const*         name_tp = "gsl";
    int                 ev_req_tp[] = { EV_SYN, EV_ABS, -1};
    int                 cd_abs_req[] = { ABS_MT_SLOT, ABS_MT_TRACKING_ID, ABS_MT_POSITION_X, ABS_MT_POSITION_Y, -1 };

    // fdTP <--
    if ( ( devname = CALL(scan_dev_event, name_tp, ev_req_tp, cd_abs_req, EV_ABS, ABS_CNT ) ) == 0 ||
         ( fdTP = CALL(open, devname, O_RDONLY)) < 0 
        ) {
        TST_ERR("getting touch pannel failed\n");
        rc = -errno;
        //goto ret;
    }

#ifdef TST_USE_ENQUEUE_KEY    
    // uses RecoveryUI::EnqueueKey
    PUSH_EVENT_FNC  fnc_push_event = [this](int code) {
        this->EnqueueKey(code);
    };
#else

    int                 fdKey = -1; // fd for keypad
    //...requirement for keypad
    char const*         name_key = "keypad";
    int                 ev_req_key[] = { EV_SYN, EV_KEY, -1};
    int                 cd_key_req[] = { KEY_VOLUMEUP, KEY_VOLUMEDOWN, -1};

    // fdKey <--
    if ( rc ||
         ( devname = CALL(scan_dev_event, name_key, ev_req_key, cd_key_req, EV_KEY, KEY_CNT)) == 0 ||
         ( fdKey = CALL(open, devname, O_WRONLY)) < 0 
        ) {
        TST_ERR("getting keypad failed\n");
        rc = -errno;
        if ( fdKey >= 0 )
            close(fdKey);
        //goto ret;
    }
    
    // write directly into fdKey
    PUSH_EVENT_FNC  fnc_push_event = [fdKey](int code) {
        struct input_event  events[] = {
            { {0,0}, EV_KEY, (unsigned short)code, 1 },
            { {0,0}, EV_SYN, 0, 0 },
            { {0,0}, EV_KEY, (unsigned short)code, 0 },
            { {0,0}, EV_SYN, 0, 0 },
        };
        int   rc = 0;
        for ( int i = 0 ; i < (int)ARRAY_SIZE(events) ; i ++ ) {
            struct timespec     timespec;
            int                 wt;
            clock_gettime(CLOCK_REALTIME, &timespec );
            events[i].time.tv_sec = timespec.tv_sec;
            events[i].time.tv_usec = timespec.tv_nsec/1000;
            if ( (wt = write(fdKey, &events[i], sizeof(events[i]))) != sizeof(events[i]) ) {
                rc = -errno;
                break;
            }
        }
        if (rc)
            TST_ERR("push_key_event: rc:%d(%s)\n", rc, strerror(errno));
    };
#endif

    if ( ! rc ) {
        // start input thread
        auto  fnc_thread = [fdTP,fnc_push_event]() {
            convert_swipe( fdTP, fnc_push_event ); // do not return
            return nullptr;
        };
        input_thread_ = std::thread( fnc_thread );
    } else {
        TST_ERR("starting thread failed %d(%s)\n", errno, strerror(errno));
        if ( fdTP >= 0 )
            close(fdTP);
    }
}
