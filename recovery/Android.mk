ifneq ($(filter t-station,$(TARGET_DEVICE)),)
LOCAL_PATH := $(call my-dir)

# Initialize some variables with null string
# to prevent deffered evaluation of the right hand side of '+=' operator.
# According to the GNU Make manual:
#   For the append operator '+=', the right-hand side is considered immediate
#   if the variable was previously set as a simple variable (‘:=’ or ‘::=’),
#   and deferred otherwise.
# ( LOCAL_xxx are cleared by include $(CLEAR_VARS) )
TMP_DEPS :=
TMP_EXECUTABLES :=
TMP_SHARED_LIBS :=
BOARD_RECOVERY_IMAGE_PREPARE :=


##########################
# recovery_extra_for_tstation
##########################
include $(CLEAR_VARS)
LOCAL_MODULE := recovery_extra_for_tstation



# For installing open_gapps.
#... copy busybox and
#... make symlink /sbin/xxx -> /sbin/busybox
TMP_BB_APPLETS := $(PRODUCT_OUT)/utilities/busybox-applets.list
LOCAL_ADDITIONAL_DEPENDENCIES += busybox-aosp $(TMP_BB_APPLETS)
LOCAL_POST_INSTALL_CMD += \
	mkdir -p $(TARGET_RECOVERY_ROOT_OUT)/sbin ; \
	cp -f $(PRODUCT_OUT)/utilities/busybox-aosp $(TARGET_RECOVERY_ROOT_OUT)/sbin/busybox ; \
	for ap in `cat $(TMP_BB_APPLETS)` ; \
	do ln -sf /sbin/busybox $(TARGET_RECOVERY_ROOT_OUT)/sbin/$$ap ; done ;
TMP_DEPS += \
	$(TARGET_RECOVERY_ROOT_OUT)/sbin/busybox \
	$(TARGET_RECOVERY_ROOT_OUT)/sbin/sh
#...symlink /etc/fstab -> /etc/recovery.fstab
LOCAL_POST_INSTALL_CMD += \
	mkdir -p $(TARGET_RECOVERY_ROOT_OUT)/etc ; \
	ln -sf /etc/recovery.fstab $(TARGET_RECOVERY_ROOT_OUT)/etc/fstab ;
TMP_DEPS += \
	$(TARGET_RECOVERY_ROOT_OUT)/etc/fstab
#... make /etc/gapps-config.txt
GAPP_CONFIG := $(TARGET_RECOVERY_ROOT_OUT)/etc/gapps-config.txt
LOCAL_POST_INSTALL_CMD += \
	mkdir -p $(dir $(GAPP_CONFIG)) ; \
	rm -f $(GAPP_CONFIG) ; \
	for ln in PreOdex Include '' ; do echo $$ln >> $(GAPP_CONFIG) ; done ;
TMP_DEPS += $(GAPP_CONFIG)



# Copy some files from system image ( system/bin, system/lib ).
#...for debugging recovery image.
TMP_EXECUTABLES += sh linker
TMP_SHARED_LIBS += \
		libc++.so \
		libc.so \
		libdl.so \
		libm.so
TMP_EXECUTABLES += e2fsck mke2fs
TMP_SHARED_LIBS += \
		libext2_blkid.so \
		libext2_com_err.so \
		libext2_e2p.so \
		libext2_profile.so \
		libext2_quota.so \
		libext2_uuid.so \
		libext2fs.so
#...for open_gappps installation
TMP_EXECUTABLES += logd logcat
TMP_SHARED_LIBS += \
		libsysutils.so \
		liblog.so \
		libcutils.so \
		libbase.so \
		libpackagelistparser.so \
		libnl.so \
		libpcre.so \
		libpcrecpp.so
# TMP_EXECUTABLES += toolbox toybox
# TMP_SHARED_LIBS += \
# 		libcrypto.so \
# 		libcutils.so \
# 		liblog.so \
# 		libpackagelistparser.so \
# 		libpcre.so \
# 		libselinux.so \
# 		libstdc++.so
LOCAL_REQUIRED_MODULES := $(TMP_EXECUTABLES) $(TMP_SHARED_LIBS)
LOCAL_POST_INSTALL_CMD += \
	mkdir -p $(TARGET_RECOVERY_ROOT_OUT)/system/bin ; \
	mkdir -p $(TARGET_RECOVERY_ROOT_OUT)/system/lib ; \
	for f in $(TMP_EXECUTABLES) ; do cp $(TARGET_OUT)/bin/$$f $(TARGET_RECOVERY_ROOT_OUT)/system/bin/ ; done ; \
	for f in $(TMP_SHARED_LIBS) ; do cp $(TARGET_OUT)/lib/$$f $(TARGET_RECOVERY_ROOT_OUT)/system/lib/ ; done ;
TMP_DEPS += $(patsubst %,$(TARGET_RECOVERY_ROOT_OUT)/system/bin/%,$(TMP_EXECUTABLES))
TMP_DEPS += $(patsubst %,$(TARGET_RECOVERY_ROOT_OUT)/system/lib/%,$(TMP_SHARED_LIBS))



# LOCAL_POST_INSTALL_CMD is executed when
#   (1) Android.mk or
#   (2) LOCAL_ADDITIONAL_DEPENDENCIES
#		is updated.
#
# So
# update LOCAL_ADDITIONAL_DEPENDENCIES( == ./timestamp )
# when the target files are not exist ( maybe manually deleted ).
TIMESTAMP := $(LOCAL_PATH)/timestamp
TMP_DEPS += $(TIMESTAMP)
DUMMY := $(shell for f in $(TMP_DEPS) ; do [ ! -e $$f ] && [ ! -L $$f ] && echo $$f > $(TIMESTAMP) && break ; done)
LOCAL_ADDITIONAL_DEPENDENCIES += $(TIMESTAMP)



# Modify init.rc in recovery ramdisk as below:
#   'start ueventd' line is moved
#     from 'on early-init' section
#     to   'on boot' section ;
# Because in recovery mode
#   (1) healthd starts just after ueventd starts and it tries to write to /dev/kmsg,
#   (2) but at this point the node /dev/kmsg is not created by ueventd yet,
#   (3) so tries to mknod( major=1(mem device), minor=11(kmsg) ) with name /dev/__kmsg__
#       in system/core/libcutils/klog.c,
#   (4) this access is not allowed in sepolicy.
BOARD_RECOVERY_IMAGE_PREPARE += $(LOCAL_PATH)/../modify-initrc.sh $(TARGET_RECOVERY_ROOT_OUT)/init.rc ;



#0805
# Revert not to delete the files.
# Because:
# In normal mode ( ie. non recovery mode ), there is some situation in which the init process
# writes some message into /misc partition (via write_bootloader_message )
# and reboot into recovery mode.
# ( ref 
#		system/vold/cryptfs.c
#		system/core/init/builtins.cpp )
# And then in recovery mode, recovery process reads what to do from /misc partition ( via read_bootloader_message ).
# write_bootloader_message and read_bootloader_message use fstab.${ro.hardware} to get /misc partition,
# because fstab.${ro.hardware} is used in normal mode.
#
# # Delete un-used files
# BOARD_RECOVERY_IMAGE_PREPARE += rm $(TARGET_RECOVERY_ROOT_OUT)/fstab.rk30board* ;
# # 0403 removed (fstab.rk30board and fstab.rk30board.bootmode.unknown are not used in recovery mode.)
# #      But some command (like swapon) require fstab.rk30board. so leave it here.
# # # symlink fstab.rk30board -> fstab.rk30board.bootmode.unknown
# # LOCAL_POST_INSTALL_CMD += \
# # 	mkdir -p $(TARGET_RECOVERY_ROOT_OUT) ; \
# # 	ln -sf /fstab.rk30board.bootmode.unknown $(TARGET_RECOVERY_ROOT_OUT)/fstab.rk30board ;
# # TMP_DEPS += \
# # 	$(TARGET_RECOVERY_ROOT_OUT)/fstab.rk30board.bootmode.unknown \
# # 	$(TARGET_RECOVERY_ROOT_OUT)/fstab.rk30board



include $(BUILD_PHONY_PACKAGE)


#include $(LOCAL_PATH)/../copy-files.mk
#$(call private_copy_files,$(TMP_EXECUTABLES),bin,$(TARGET_RECOVERY_ROOT_OUT)/system/bin,root_)
#$(call private_copy_files,$(TMP_SHARED_LIBS),lib,$(TARGET_RECOVERY_ROOT_OUT)/system/lib,root_)



##########################
# librecovery_ui_tstation
##########################
include $(CLEAR_VARS)

# should match TARGET_RECOVERY_UI_LIB set in BoardConfig.mk
LOCAL_MODULE := librecovery_ui_tstation
LOCAL_CLANG := true
LOCAL_C_INCLUDES += \
		bootable/recovery \
		system/core/include/system

LOCAL_SRC_FILES := recovery_ui.cpp
include $(BUILD_STATIC_LIBRARY)


##########################



endif
